const config = [
    {
        name: 'SoR',
        data: {
            name: 'Song of Rest',
            hint: 'Enables the Song of Rest script',
            scope: 'world',
            config: true,
            type: Boolean,
            default: false,
        },
    },
    {
        name: 'UFort',
        data: {
            name: 'Undead Fortitude',
            hint: 'Auto prompt for Undead Fortitude checks',
            scope: 'world',
            config: true,
            type: Boolean,
            default: false,
        },
    },
    {
        name: 'FRes',
        data: {
            name: 'Fiendish Resiliance',
            hint: 'Enable Fiendish Resiliance notification upon resting',
            scope: 'world',
            config: true,
            type: Boolean,
            default: false,
        },
    },
];
Hooks.once('init', () => {
    // Register settings
    config.forEach((cfg) => {
        game.settings.register('FA5e', cfg.name, cfg.data);
    });
});
