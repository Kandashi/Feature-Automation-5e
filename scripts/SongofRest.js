const scope = 'FA5e';
if (game.settings.get('world', 'SoR')) {
    const song = ''
    Hooks.on("updateScene", () => {
        let pcs = game.actors.filter(e => e.data.type === "character" && e.isPC);
        let song = pcs.items.find(i => i.name === "Song of Rest");
        if (song) {
            console.log("Actor with Song of Rest found")
        }
    });

    if (song) {
        Hooks.on(`renderShortRestDialog`, (actorData, dialog, content) => {
            let actor = game.actors.get(actorData.actor.data._id)
            if (actor.getFlag(scope, 'SongOfRestComplete')) {
                ChatMessage.create({ content: actor.name + "has already used the Song of Rest" })
                return;
            }
            Hooks.once(`renderDialog`, (dialog, html) => {
                if (dialog.data.title === `Roll Hit Dice`) {
                    html.find('[name=bonus]')[0].value = `+1d6`;
                    actor.setFlag(scope, 'SongOfRestComplete', 'yes')
                }
            });

        });


        Hooks.on('closeShortRestDialog', (actorData, content) => {
            let actor = game.actors.get(actorData.actor.data._id)
            actor.unsetFlag(scope, 'SongOfRestComplete')

        });
    }
}